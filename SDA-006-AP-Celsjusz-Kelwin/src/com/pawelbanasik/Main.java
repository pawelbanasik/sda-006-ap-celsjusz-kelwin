package com.pawelbanasik;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("What is input unit? (k/c)");
		String line = scan.nextLine();
		if (line.equals("k")) {
			System.out.println("Please input temp in Kelvin: ");
			
			double kelvinTemp = scan.nextDouble();
			double celsiusTemp = kelvinTemp - 273.15;

			System.out.println("celsius: "+ celsiusTemp);
			
		} else if (line.equals("c")) {
			System.out.println("Please input temp in Celsiuses: ");
			
			double celsiusTemp = scan.nextDouble();
			double kelvinTemp = celsiusTemp + 273.15;
			System.out.println("kelvins: "+ kelvinTemp);
		}
	}

}
